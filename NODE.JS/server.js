var net = require('net');
// подключаем перекодировщик строк
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');

// порт сервера
var _port = 8000;
// счетчик клиентов
var IDcounter = -1;

// функция рандома int
function RandomInt(min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
// функция рандома float
function RandomFloat(min, max){
	return (Math.random() * (max - min + 1)) + min;
}

// массив подключенных клиентов
var clients = [];

// класс клиента
function Client(_socket, _id){
	this.nickname = '<noname>';
	this.socket = _socket; // ссылка на сокет клиента
	this.ID = _id; // уникальный ID
}

// подключение клиента
var s = net.Server(function (socket) {
	console.log("Client connected");
	IDcounter++; // счетчик плюсуем
	_client = new Client(socket, ""+IDcounter); // создаем класс клиента
	_client.socket.write('["SYSTEM","ID","'+ IDcounter +'"]'); // отправляем ему ID
	clients.push(_client); // добавляем в массив клиентов

	// при получении данных
	socket.on('data', function (msg) {
		var msg2 = ''+msg; // приводим вход к строке
		msg2 = decoder.write(msg); // декодируем из UTF8
		console.log("Input: " + msg2); 
		data = "" + msg2; // опять приводим к строке
		var j = JSON.parse(data); // распарсиваем JSON
		// это вывод всех ключей по одному
		for (var i = 0; i<j.length; i++){
			console.log("	#"+i+" = "+j[i]);
		}
		
		// если первый ключ MESSAGE
		if (j[0] === 'MESSAGE'){
			clients.forEach( function(c){ // перебираем всех клиентов в массиве
				c.socket.write(msg);  // и посылаем им входящий пакет
				console.log('Message: ' + msg2);
			});
		}

		// если первый ключ SYSTEM
		if (j[0] === 'SYSTEM'){
			if (j[1] === 'NICKNAME'){ // если второй ключ NICKNAME
				clients.forEach( function(c){ // перебираем клиентов
					if (c.ID === j[2]){ // если третий ключ совпал с ID клиента - мы нашли отправителя
						c.nickname = j[3]; // задаём ему ник, который он прислал
						console.log(c.ID + ' set nick ' + c.nickname + ' ('+j[3]);
					}
				})
			}
			if (j[1] === 'LIST'){ // если второй ключ LIST
				var s = '["SYSTEM","USERS","'+j[2]+'","'; // формируем начало строки
				clients.forEach( function(c){ // перебираем всех клиентов
					s += c.nickname+'\n'; // добоавляем к строке ник и перевод строки
				});
				s += '"]'; // завершаем строку
				console.log('== '+s);
				clients.forEach( function(c){ // перебираем всех клиентов
					if (c.ID === j[2]) { // если третий ключ совпал с ID клиента - мы нашли отправителя
						c.socket.write(s); // отправляем ему сформированную строку со списком клиентов
						console.log('Message to: ' + j[2] + ' (' + c.ID + ') ' + s);
					}
				});
	
				}
		}

	});

	// чтобы не вылетело в случае некорректного отключения клиента
	socket.on('error', function() {});

	// при отключении клиента
	socket.on('end', function () {
		console.log("Client disconnected");
		var i = clients.indexOf(socket); // находим отключившегося по сокету
		var nick = "";
		for (var i in clients) // перебираем всех клиентов
			if (clients[i].socket === socket) { // найденного по сокету 
				nick = clients[i].nickname; // запмним имя отключенного
				clients.splice(i, 1); // удаляем из массива клиентов
			}

		for (var i in clients) { // перебираем всех клиентов
			try {
				clients[i].socket.write("[""MESSAGE"",""SERVER"",""-1"",""" + nickname + " user left""]"); // отправляем строку
			}
			catch (ex) {}
		}
	});
});

s.listen(_port);

//* Handle ctrl+c event
process.on('SIGINT', function () {
	for (var i in clients) {
		clients[i].write("Server shutdown");
		clients[i].destroy();
	}
	console.log('Exiting properly');
	process.exit(0);
});

console.log("Chat Server listening " + s.address().address + ":" + s.address().port);